#!/bin/bash
# Create new MySQL database.

# Environment-configurable variables
MYSQLUSER=root
MYSQLPASS=

if [ $# -lt 1 ]
then
  echo "USAGE: $0 projectname";
  echo "  New MySQL database requires one argument (the name of the project to create a development database for)."
  return
fi

MYSQL=`which mysql`
Q1="CREATE DATABASE IF NOT EXISTS $1;"
Q2="GRANT ALL ON *.* TO '$1'@'localhost' IDENTIFIED BY '$1';"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"
 
$MYSQL -u$MYSQLUSER -p$MYSQLPASS -e "$SQL"

echo "[@TODO check for no error before claiming this] A database and user have been created with the database name, user name, and password all set to $1."
