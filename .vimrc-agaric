" This file is called from .vimrc (otherwise empty) with the following line:
" source ~/Projects/mlncn/scripts/.vimrc-agaric

:filetype plugin on

set clipboard^=unnamed,unnamedplus
set showcmd
set incsearch
set expandtab
set tabstop=2 shiftwidth=2 softtabstop=2
set autoindent
" set foldmethod=indent
set smartindent

" Set python and JavaScript files to four space indents
autocmd FileType python setlocal tabstop=4 shiftwidth=4 softtabstop=4
autocmd FileType javascript setlocal tabstop=4 shiftwidth=4 softtabstop=4

if has("autocmd")
  augroup phpfiles
    autocmd BufRead,BufNewFile *.php,*.module,*.theme,*.inc,*.install,*.info,*.engine,*.profile,*.test,*.twig set filetype=php
  augroup END
endif

syntax on

let php_sql_query = 1
let php_baselib = 1
let php_htmlInStrings = 1
" let php_folding = 1

" Highlight chars that go over the 80-column limit
" :highlight OverLength ctermbg=darkgrey guibg=darkgrey
" :match OverLength '\%81v.*'

" set make command when editing php files for simple syntax checking.
set makeprg=php\ -l\ %
set errorformat=%m\ in\ %f\ on\ line\ %l

" PHP Syntax checking
cabbr pc !php -l %

" set autohighligting of brackets already done?

" autocomplete functions and identifiers for languages
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

" make normal copy-paste shortcuts work
" map ^P "+gP
" map ^C "+y

" source $VIMRUNTIME/macosx.vim
" behave macosx

" vim-gnome needs to be installed for there to be a mswin file!
source $VIMRUNTIME/mswin.vim
behave mswin

" VimBall stuff needs to be set up for this to work.
" source /home/ben/customhome/supertab.vba

" See http://data.agaric.com/node/3661
set tags=ctagstags;/

" -- something in Janus or whatever now handles this --
" When you need to save as sudo... capital W to the rescue!
" command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

:hi gitcommitSummary ctermfg=white ctermbg=black

autocmd FileType markdown highlight
:hi Title ctermfg=black

" See https://www.reddit.com/r/vim/comments/3vmu1l/markdown_syntax_with_boldcolored_headers/
autocmd FileType markdown highlight Title cterm=bold ctermfg=black ctermbg=white guifg=#EEEEEC
" ctermbg would need to be numeric, 256 color option; this breaks: ctermbg=#EEEEEC
