" This file is called from .vimrc (otherwise empty) with the following line:
" source ~/Projects/mlncn/scripts/.vimrc
" Or on NixOS for Neovim, put the above in `~/.config/nvim/init.vim`


" if exists('g:vscode')
  " VSCode extension
" else
  " Ordinary neovim
" endif

set nocompatible

" Share one clipboard across Vim/NeoVim instances
:set clipboard+=unnamedplus

" Make normal copy-paste commands work in Vim, pasting from the clipboard
" https://superuser.com/questions/10588/how-to-make-cut-copy-paste-in-gvim-on-ubuntu-work-with-ctrlx-ctrlc-ctrlv

vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

" CTRL-Z is Undo; not in cmdline though
noremap <C-Z> u
inoremap <C-Z> <C-O>u
" CTRL-Y is Redo (although not repeat); not in cmdline though
noremap <C-Y> <C-R>
inoremap <C-Y> <C-O><C-R>
" CTRL-Tab is Next window
noremap <C-Tab> <C-W>w
inoremap <C-Tab> <C-O><C-W>w
cnoremap <C-Tab> <C-C><C-W>w

" Allow the mouse to be used within terminal vim.
" There is now officially no need for gvim.
" Thanks to Chris Thompson and http://vim.wikia.com/wiki/Using_the_mouse_for_Vim_in_an_xterm
" To keep copy-paste working use -r not -a https://unix.stackexchange.com/a/140584/171876
" Alternate solution see http://vim.wikia.com/wiki/Make_mouse_drag_not_select_text_or_go_into_visual_mode
" 'nicr' works well in NeoVim; it is necessary to press shift to select text for non-Vim-style copying
" :set mouse=nicr
" :set mouse=
:set mouse=ar

set tabstop=2
set shiftwidth=2
set expandtab

set whichwrap+=<,>,[,]


if &compatible
  set nocompatible
endif
