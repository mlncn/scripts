#!/bin/sh -e

if [ $# -lt 2 ]; then
    echo "Usage: $0 oldversion newversion (optional) directory (e.g. 5.5 5.6 dir)"
    exit 1
fi

# If you change TMP, you will have to change the -p3 option in the patch command!
TMP=/tmp
# Change the version below to what you have
VER_OLD=$1
VER_NEW=$2

if [ $# -gt 2 ]; then
  DRUPAL_DIR=$3
else
  DRUPAL_DIR=`pwd`
fi

# Change that to the directory where Drupal is installed
PATCH_FILE=$TMP/drupal-$VER_OLD-to-$VER_NEW.patch
cd $TMP

# Download your current version
wget http://ftp.drupal.org/files/projects/drupal-$VER_OLD.tar.gz

# Extract it
tar -xzf drupal-$VER_OLD.tar.gz

# Now, download the new version
wget http://ftp.drupal.org/files/projects/drupal-$VER_NEW.tar.gz
# And extract that too
tar -xzf drupal-$VER_NEW.tar.gz

# Now create the diff file
# echo "This command, or the next one, breaks the script so you'll just have to do the rest yourself."
echo `diff -Naur $TMP/drupal-$VER_OLD $TMP/drupal-$VER_NEW > $PATCH_FILE`

# Now change to the directory where your Drupal installation is
cd $DRUPAL_DIR

# we'll want to see the output for this
set -vx

# Check that the patch would apply without errors
patch -p3 --dry-run < $PATCH_FILE

# turn off verbose output
set +vx
# turning it back off (naturally, on is a minus sign and off is a plus sign)

echo "If the above dry run patch applied without errors, you can press Y to apply the patch for real."
echo "If there are errors, or you just aren't ready to apply the patch, press N to abort."
read YN
if ( test -z "$YN" )
then
  echo -e "Please enter either \"Y\" or \"N\" " ;
  eval "$0" "$@" ;
  exit ;
fi
# at this point 'YN' contains Y, y, N, or n
if ( test "$YN" = "N" -o "$YN" = "n" )
then
  exit ;
fi

set -vx
# Assuming there are no error from the previous step, you can
# now apply the patch for real
patch -p3 < $PATCH_FILE
