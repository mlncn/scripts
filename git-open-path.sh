#!/bin/bash
# Depends on: https://github.com/paulirish/git-open
# See also: https://github.com/paulirish/git-open/issues/54

# From TylerRick at https://github.com/paulirish/git-open/issues/54#issuecomment-404259397
# Requires https://github.com/paulirish/git-open be installed.
# git-open-file() {
  BRANCH=""
# I think there's a bug in git-open and it shouldn't be truncating '/tree/master'
# but not, for example, '/tree/8.x-1.x' when *that* is the default branch.
#  if [ "$(git symbolic-ref --short HEAD)" == "$(git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@')" ]; then
#    local BRANCH="/tree/$(git symbolic-ref --short HEAD)"
#  fi
# But given that that is the way it is working here's the workaround code:
  if [ "$(git symbolic-ref --short HEAD)" == "master" ]; then
    BRANCH="/tree/master"
  fi
  BASE="$(BROWSER=echo git open)$BRANCH"
  if [[ ${BASE:0:23} == "https://git.agaric.com/" ]]; then
    BASE=${BASE/\/tree\//"/src/"}
  fi
  if [ ! -d "$1" ]; then
    BASE="${BASE/tree/blob}"
  fi
  xdg-open "$BASE/$(git rev-parse --show-prefix)$1"
# }

# xdg-open "$(BROWSER=echo git open)/-/blob/$(git symbolic-ref --short HEAD)/$@"
