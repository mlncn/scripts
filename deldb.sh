#!/bin/bash
# Database delete

# Environment-configurable variables
MYSQLUSER=root
MYSQLPASS=toor

if [ $# -lt 1 ]
then
  echo "USAGE: $0 projectname";
  echo "  Database delete fetch requires one argument (the name of the project to delete a development database for)."
  return
fi

MYSQL=`which mysql`
Q1="DROP USER '$1'@'localhost';"
Q2="DROP DATABASE IF EXISTS $1;"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"
 
$MYSQL -u$MYSQLUSER -p$MYSQLPASS -e "$SQL"

echo "[@TODO check for no error before claiming this] A database and user with the name $1 have been deleted."
