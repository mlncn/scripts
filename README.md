## Installation

```
mkdir -p ~/Projects/mlncn
git clone git@gitlab.com:mlncn/scripts.git ~/Projects/mlncn/scripts
cd ~/Projects/mlncn/scripts
```

Not yet incorporated into setup.sh; add the below to your .bashrc:

```
if [ -f ~/Projects/mlncn/scripts/.bash_custom ]; then
    . ~/Projects/mlncn/scripts/.bash_custom
fi
```

You can do that with:

```
echo "
if [ -f ~/Projects/mlncn/scripts/.bash_custom ]; then
    . ~/Projects/mlncn/scripts/.bash_custom
fi
">> ~/.bashrc 
```
