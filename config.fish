# Add this line to your `~/.config/fish/config.fish` file inside `if status is-interactive`
# source ~/Projects/mlncn/scripts/config.fish

# source ~/Projects/mlncn/scripts/config-homebrew.fish

zoxide init fish | source

# Hide the fish message, which is by default the only-need-to-see-this-once:
# Welcome to fish, the friendly interactive shell
# Type help for instructions on how to use fish
set -U fish_greeting

# alias for using default program to open files
# used to be gnome-open but https://ubuntuforums.org/showthread.php?t=2124061
alias o='xdg-open'

# Goodby Gvim, hello non-graphical but just fine NeoVim
# Actually NeoVim isn't bringing anything except helping convince me to dump
# gvim.  Let's try sticking with vim.  SEE ALSO ~/.gitconfig in this file.
# Back to NeoVim; it makes clipboard work across instances and changes cursor.
alias v='nvim'

alias pom="cd /home/mlncn/Projects/agaric/pomodoroprompt && nix-shell --command 'python pomodoroprompt.py'"

# Not everything that was in .bash_custom but should be good enough
abbr -a -- gof 'git-open-path'

# Git command aliases to speed up regularly used ones
abbr -a -- gs 'git show'
abbr -a -- gst 'git status'
# Update and remove in git all modified and deleted files in working copy.
abbr -a -- gu 'git add -u'
abbr -a -- ga 'git add -A'
abbr -a -- gar 'GIT_EXTERNAL_DIFF=diffr git add -p'
# drupal-ready diff - run from relevant root directory
abbr -a -- gd 'git diff --submodule'
# Diff that ignores config files whose only change is the UUID (ignores lines
# starting with 'uuid:' and probably other stuff but it's solid so far.)
abbr -a -- gdu "git diff -G '^[^u]*([^u][^i][^d][^:]).*'"
# See diff of what has been git added / removed so far.
abbr -a -- gdc 'git diff --cached --submodule'
# Get first line in but then immediately go to edit.
function gce
  git commit -m "$argv" && git commit --amend
end
abbr -a -- gcm 'git commit -m'
abbr -a -- gca 'git commit --amend'
abbr -a -- gcma 'git commit -m --amend'
abbr -a -- gc 'git commit'
abbr -a -- gccl 'git add -A composer.lock && git commit -m "Update composer lock file"'
abbr -a -- gco 'git checkout'
abbr -a -- gg 'git gui &'
# Pretty Git Log
abbr -a -- gl 'git log --pretty=format:"%cd %an %s %h" --date=short'
# Pretty git log for just me for the past day
abbr -a -- gdlp 'git log --pretty=format:"%s." --author="benjamin melançon" --since=yesterday --reverse'
# Pretty git log for just me for past two weeks
abbr -a -- glp 'git log --pretty=format:"%cd %s." --date=short --author="benjamin melançon" --since="2 weeks ago" --reverse'
# Pretty git log complete (with message body) for just me for past five weeks.  Use %n for newline instead of that period
abbr -a -- glpc 'git log --pretty=format:"%cd %s. %b" --date=short --author="benjamin melançon" --since="2 weeks ago" --reverse'
# Shortlog
abbr -a -- glo 'git log --oneline'
abbr -a -- gb "git for-each-ref --sort=committerdate refs/heads/ --format='%(committerdate:short) %(refname:short)'"
abbr -a -- gr "git ls-files --deleted | xargs git rm" # Same but doesn't work as alias: git rm $(git ls-files --deleted)
# stage matching files interactively (when you have hundreds and don't want to
# skip the first hundred manually).  Use like this:
# git-add-from <unique string identifying first file>
function git-add-from
  git diff --name-only|grep -A999 $argv| xargs -po git add -p
end
# handle the really annoying failure of git to delete removed directories
function grm
  git rm -r $argv
  rm -rf $argv
end
function gclo
  if test (count $argv) -eq 1
    git clean $argv
    git checkout -- $argv
  else
    echo "Please provide a path; to use * wildcard expansion quote your argument. (Fish is kinda fascist about this.)  This command (git clean plus git checkout) is too dangerous to run for your whole directory; say what you're aiming for."
    # See https://github.com/fish-shell/fish-shell/issues/8926 for where they force the user to do the quoting.
  end
end

# http://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git
abbr -a -- glg 'git log --graph --full-history --all --color --pretty=tformat:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m"'
abbr -a -- p 'git push --recurse-submodules=check'
# Safer git force push, only forces if overwriting your *own* pushed code (or you've addressed others' pushes)
abbr -a -- pf 'git push --force-with-lease --force-if-includes --recurse-submodules=check'
# For first push of a new branch, with git no longer autocreating remote.
# See http://www.janosgyerik.com/easy-way-to-git-push-the-current-branch/
abbr -a -- pu 'git push -u origin HEAD'
abbr -a -- pl 'git pull -s recursive -Xpatience --recurse'
abbr -a -- co 'git checkout'
abbr -a -- cop 'git checkout -p'
abbr -a -- gap 'git add -AN && git add --patch'
abbr -a -- gdtag 'git tag $(date +%Y%m%dT%H%M%Z)' # <- must be single quotes or time is cached
abbr -a -- pt "git push --tags"
abbr -a -- gtags "git log --tags --no-walk --date=format:'%Y-%m-%d' --pretty='%C(auto)%h %cd%d %s'"
function gtagdel
  git tag --delete $argv
  git push --delete origin $argv
end

function uut
  set -f raw $argv[1]
  if test "$raw" -gt 16649225790
    echo "We're dealing with milliseconds; converting…";
    set unix_time -f $raw/1000
  else
    set unix_time -f $raw;
  end
  echo $(date -I -d @$unix_time);
end

# Listing and other basic shell commands
# OLD: abbr -a -- l 'ls -lhaF --color=auto --show-control-chars'
abbr -a -- l 'ls -AghG1 --time-style="+%y-%m-%d %k:%M" --group-directories-first'
# TODO see if i can come to a resolution with https://gitlab.com/agaric/raw-notes/blob/master/content/notes/2019-08-03-best-listing-of-directory-and-file-information.md

abbr -a -- ll 'ls -AghG1 --time-style="+%y-%m-%d %k:%M" --sort=time --reverse'

# Sort by *local* file creation
abbr -a -- lc 'ls -AghG1tr --time=birth --time-style="+%y-%m-%d %k:%M"'

# grep recursive excluding hidden directories, binary files, metadata
abbr -a -- gpe 'grep -nHIRs --exclude-dir=".git" --exclude-dir=".metadata" --exclude-dir=".idea" --exclude-dir="WEB-INF" --exclude-dir="build" --exclude-dir="_*" --exclude="*.min.*" --exclude="*.log" --exclude-dir="data" --exclude-dir="backups" --exclude-dir="site-backups"'

abbr -a -- gp 'grep -nHIRs'

# Copy to clipboard; useful to pipe output to clipboard: d uli | x
abbr -a -- x 'xclip -sel c'

# did not bring tcd over from .bash_custom

# From https://unix.stackexchange.com/a/709257/171876
function mkd -d "Create a directory and set CWD"
    command mkdir -p $argv
    if test $status = 0
        switch $argv[(count $argv)]
            case '-*'

            case '*'
                cd $argv[(count $argv)]
                return
        end
    end
end

# remove safe: mv to ~/.Trash rather than deleting outright.
# Obsolete now that we have trash installed.
# function trash
#   mv $argv ~/.Trash/
# end

# big section here that we did not convert from .bash_custom but mostly about terminal colors?
# oh but also prompt command history


abbr -a -- writeme 'php ~/Projects/drutopia/writeme/src/writeme.php'
abbr -a -- matchcomposer 'cp ~/Projects/drutopia-platform/build_source/composer.* . && git add composer.json && git commit -m "Update composer.json to match build source" && git add composer.lock && git commit-m "Update composer lock file to match buid source"'

# For convenience using Drush and ddev
abbr -a -- d 'ddev drush'
abbr -a -- dy 'ddev drush -y'
abbr -a -- dtd "ddev drush php:eval \"\Drupal::keyValue('development_settings')->setMultiple(['twig_debug' => TRUE, 'twig_cache_disable' => TRUE]);\" && ddev drush cache:rebuild"
abbr -a -- dtdd "ddev drush php:eval \"\Drupal::keyValue('development_settings')->setMultiple(['twig_debug' => FALSE, 'twig_cache_disable' => FALSE]);\" && ddev drush cache:rebuild"
# Drush disable aggregation
abbr -a -- dda "ddev drush -y cset system.performance css.preprocess 0 && ddev drush -y cset system.performance js.preprocess 0 && ddev drush cache:rebuild"
# Drush enable aggregation
abbr -a -- dea "ddev drush -y cset system.performance css.preprocess 1 && ddev drush -y cset system.performance js.preprocess 1 && ddev drush cache:rebuild"
abbr -a -- dco 'ddev composer'
abbr -a -- dc 'ddev composer'
abbr -a -- ds 'ddev start && ddev auth ssh'
abbr -a -- dcr 'ddev composer require'
abbr -a -- dcu 'ddev composer update'

# For convenience getting audio from YouTube videos
# We have replaced youtube-dl with yt-dlp which releases bugfixes faster.
abbr -a -- ya 'yt-dlp -x'
abbr -a -- yas 'yt-dlp -x --split-chapters'

# For convenience using Flatpak applications with command line component
abbr -a -- fr 'flatpak run --command='
# See also (place in .local/bin/ but change first line to your actual bash path, not sh)
# https://opensource.com/article/21/5/launch-flatpaks-linux-terminal

# Show all Docker containers and their port mappings
abbr -a -- docker-ports "docker container ls --format \"table {{.ID}}\t{{.Names}}\t{{.Ports}}\" -a"


# did not convert from .bash_custom:
# export PATH "$PATH:$HOME/.config/composer/vendor/bin"


# Courtesy Keegan!
abbr -a -- compose-list 'less "/usr/share/X11/locale/$(grep --max-count=1 "${LANG%.*}.UTF-8\$" /usr/share/X11/locale/locale.dir | cut --delimiter=/ --fields 1)/Compose"'

# Courtesy Chris!  @TODO put a recursive version in the drutopia_dev_template project for people who use it
# @TODO further figure out how much harder we've made it to contribute by moving to Drupal.org
# But while we cannot grant access to all Drutopia projects at once as easily the fact that regular people
# cannot push a branch to the repo immediately … huh maybe we did lose that, unless they go through the
# annoying issue branch process.  Drat.
# Less big-picture TODO: this should get the basename of the repository NOT the
# subfolder one happens to be in when running it.
function git-set-drupal-remote
  git remote set-url origin git@git.drupal.org:project/$(basename $PWD).git
end

# needs work on bash conversion
# function change_git_remote_to_ssh() {
#    set --local directory $argv
#
#    cd "$directory" || return
#
#    set current_url $(git remote get-url origin 2>/dev/null)
#
#    if [[ "$current_url"  = https://git.drupalcode.org* ]]; then
#        ssh_url $(echo "$current_url" | sed 's|^https://git.drupalcode.org/|git@git.drupal.org:|')
#        git remote set-url origin "$ssh_url"
#        echo "Changed Git remote URL to SSH in $directory"
#    fi
#}

# Need a fish version of this too
# source /home/mlncn/Projects/amanita/amanita.sh

alias amanita="just --justfile=/home/mlncn/Projects/amanita/justfile"

set fish_color_search_match white
# or does background-only mean need to say background?
# set fish_color_search_match --background white

# Somewhere i am telling fish to use Vim keybindings, but apparently the one
# thing i am used to from bash is from Emacs: `ctrl a` to go to start of line
# bind \ca beginning-of-line
# Above did not seem to work if below was set in ~/.config/fish/fish_variables
# SETUVAR fish_key_bindings:fish_vi_key_bindings
# Deleted that; will try:
function fish_hybrid_key_bindings --description \
"Vi-style bindings that inherit emacs-style bindings in all modes"
    for mode in default insert visual
        fish_default_key_bindings -M $mode
    end
    fish_vi_key_bindings --no-erase
end
set -g fish_key_bindings fish_hybrid_key_bindings
# from https://fishshell.com/docs/current/interactive.html

function fish_title
    # Show the command that is active, if any.
    set -q argv[1]; or set argv ''
    if test -n $argv
      set argv " » $argv"
    end
    echo (fish_prompt_pwd_dir_length=0 prompt_pwd)$argv;
end

# set -gx fish_prompt_pwd_dir_length 12
